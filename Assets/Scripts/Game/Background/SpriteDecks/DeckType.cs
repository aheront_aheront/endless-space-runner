﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Game.Background.SpriteDecks
{
    public class DeckType
    {
        public readonly bool CanRandomized;
        public readonly string Loadpath;

        public DeckType(string Loadpath, bool CanRandomized)
        {
            this.Loadpath = Loadpath;
            this.CanRandomized = CanRandomized;
        }
    }
}
