using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;


namespace Assets.Scripts.Game.Background.SpriteDecks
{
    public class SpriteDeck
    {
        public static readonly DeckType DEFAULT_DECK =  new DeckType("BackgroundDecks/Default", true) ;
        public static readonly DeckType DEFAULT_ANIMAL_PLANETS = new DeckType("BackgroundDeck/AnimalPlanets", true);

        private DeckType deckType;
        private Sprite[] sprites;
        private System.Random random;
        private int nextSpriteIndex;

        public SpriteDeck(DeckType deckType)
        {
            this.deckType = deckType;
            sprites = Resources.LoadAll<Sprite>(deckType.Loadpath);
            random = new System.Random();
            nextSpriteIndex = 0;
        }

        public Sprite NextSprite()
        {
            if (deckType.CanRandomized)
            {
                return sprites[random.Next(sprites.Length)];
            }   
            
            nextSpriteIndex %= sprites.Length;
            return sprites[nextSpriteIndex++];
        }
    }
}