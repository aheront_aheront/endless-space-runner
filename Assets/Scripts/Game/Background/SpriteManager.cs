using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Game.Background.SpriteDecks;

namespace Assets.Scripts.Game.Background
{
    public class SpriteManager : MonoBehaviour
    {
        
        private SpriteDeck currentDeck;
        public delegate void DeckChanged();
        public event DeckChanged OnDeckChanged;

        private void Awake()
        {
            currentDeck = new SpriteDeck(SpriteDeck.DEFAULT_DECK);
        }

        public void ChangeDeck(SpriteDeck deck)
        {
            currentDeck = deck;
            OnDeckChanged.Invoke();
        }

        public Sprite Next()
        {
            return currentDeck.NextSprite();
        }
    }
}