using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.Game.Background
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class Tile : MonoBehaviour
    {

        public Vector2 position => transform.localPosition;
        public float tileZ => Camera.main.transform.position.z + Camera.main.farClipPlane;
        public float height => boxCollider.size.y * transform.localScale.y;
        public float width => boxCollider.size.x * transform.localScale.x;

        [SerializeField] private BoxCollider2D boxCollider;
        private void Awake()
        {
            boxCollider = GetComponent<BoxCollider2D>();
        }
        public void SetSprite(Sprite sprite) => GetComponent<SpriteRenderer>().sprite = sprite;
        public void SetPosition(Vector3 position) => transform.localPosition = position;
        public void Translate(Vector2 delta) => transform.localPosition += new Vector3(delta.x, delta.y);
        public void Scale(float scale) => transform.localScale = Vector3.Scale(transform.localScale, new Vector3(scale, scale, 1));
        public bool IsOutOfScreen() => position.y < Camera.main.transform.position.y - Screen.height / 2 - height / 2;
    }
}