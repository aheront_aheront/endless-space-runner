using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.Background
{   
    [RequireComponent(typeof(SpriteManager))]
    public class TileManager : MonoBehaviour
    {

        public float TileTranslateSpeed;
        public Tile TilePrefab;

        private List<Tile> tiles;
        private SpriteManager spriteManager;
        
        private void Awake()
        {
            spriteManager = GetComponent<SpriteManager>();
            spriteManager.OnDeckChanged += RefreshTilesLayout;
            tiles = new List<Tile>();
        }

        private void Start()
        {
            TileTheScreen();            
        }

        private void TileTheScreen()
        {
            float totalTilesHeight = 0;
            int currentTileIndex = 0;
            do
            {
                if (currentTileIndex < tiles.Count)
                {
                    totalTilesHeight += tiles[currentTileIndex].height;
                    if (tiles[currentTileIndex].position.y < Camera.main.gameObject.transform.position.y)
                        totalTilesHeight += tiles[currentTileIndex].position.y;
                    currentTileIndex++;
                    continue;
                }

                Tile tile = —reateTile(this.transform);
                totalTilesHeight += tile.height;
                tiles.Add(tile);
                currentTileIndex++;
            } while (Screen.height > totalTilesHeight * (1 - 1f / tiles.Count));
        }

        private void RefreshTilesLayout()
        {
            foreach (Tile tile in tiles)
            {
                tile.SetSprite(spriteManager.Next());
            }
        }

        private void MoveTiles(float deltaTime)
        {
            foreach (Tile tile in tiles)
                tile.Translate(new Vector2(0, -Screen.height / TileTranslateSpeed * deltaTime));
        }

        private void RemoveOutOfScreenTiles()
        {
            while (tiles[0].IsOutOfScreen())
            {
                Destroy(tiles[0].gameObject);
                tiles.RemoveAt(0);
            }
        }

        private void Update()
        {
            MoveTiles(Time.deltaTime);
            RemoveOutOfScreenTiles();
            TileTheScreen();
     
        }

        private Tile —reateTile(Transform parent)
        {
            float highestTileTop;
            if (tiles.Count == 0)
                highestTileTop = Camera.main.transform.position.y - Camera.main.orthographicSize;
            else
                highestTileTop = tiles[tiles.Count-1].position.y + tiles[tiles.Count-1].height / 2;
            
            Tile tile = Instantiate(TilePrefab, parent);
            tile.Scale(Screen.width / tile.width);
            tile.SetPosition(new Vector3(Camera.main.transform.position.x, highestTileTop + tile.height / 2, tile.tileZ));
            tile.SetSprite(spriteManager.Next());
            return tile;
        }

        private void OnDestroy()
        {
            spriteManager.OnDeckChanged -= RefreshTilesLayout;
        }
    }
}