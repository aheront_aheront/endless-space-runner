﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Game.EventSystem.SwipeDetectors;

namespace Assets.Scripts.Game.EventSystem
{
    public sealed class EventSystem : MonoBehaviour
    {
        public static EventSystem instance;
        public ISwipeDetector SwipeDetector;

        private void Awake()
        {
            if (instance != null) 
                Destroy(this.gameObject);

            instance = this;

            SwipeDetector = SwipeDetectors.SwipeDetectorFactory.GetSwipeDetector();
        }

        private void Update()
        {
            SwipeDetector.UpdateSwipeState();
        }
    }
}
