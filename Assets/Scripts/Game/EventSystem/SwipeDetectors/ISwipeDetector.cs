﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Game.EventSystem.SwipeDetectors
{
    public interface ISwipeDetector
    {
        public delegate void OnSwipeInput(Vector2 direction);
        public event OnSwipeInput OnSwipe;

        public void UpdateSwipeState();
    }
}
