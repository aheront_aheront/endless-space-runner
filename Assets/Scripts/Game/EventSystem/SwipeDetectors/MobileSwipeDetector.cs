﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Game.EventSystem.SwipeDetectors
{
    public sealed class MobileSwipeDetector : SwipeDetector
    {
        public override void UpdateSwipeState()
        {
            if (Input.touchCount == 0)
                return;

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                tapStartPositon = Input.GetTouch(0).position;
                isSwiping = true;
            }

            Vector2 swipeDelta = Input.GetTouch(0).position - tapStartPositon;
            if (swipeDelta.magnitude > SwipeDetector.SWIPE_DEAD_ZONE && isSwiping)
            {
                isSwiping = false;
                RegisterSwipe(swipeDelta);
            }

            if (Input.GetTouch(0).phase == TouchPhase.Canceled ||
                    Input.GetTouch(0).phase == TouchPhase.Ended)
                isSwiping = false;
        }
    }
}
