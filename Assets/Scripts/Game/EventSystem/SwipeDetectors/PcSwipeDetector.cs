﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Game.EventSystem.SwipeDetectors
{
    public sealed class PcSwipeDetector : SwipeDetector
    {
        public override void UpdateSwipeState() 
        {
            if (Input.GetMouseButtonDown(0))
            {
                tapStartPositon = Input.mousePosition;
                isSwiping = true;
            }
            
            Vector2 swipeDelta = (Vector2) Input.mousePosition - tapStartPositon;
            if (swipeDelta.magnitude > SwipeDetector.SWIPE_DEAD_ZONE && isSwiping)
            {
                isSwiping = false;
                RegisterSwipe(swipeDelta);
            }

            if(Input.GetMouseButtonUp(0))
                isSwiping = false;
        }
    }
}
