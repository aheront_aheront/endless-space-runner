using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.EventSystem.SwipeDetectors
{
    public class SwipeDetector : ISwipeDetector
    {
        public const float SWIPE_DEAD_ZONE = 70;
        protected Vector2 tapStartPositon;
        protected bool isSwiping;
        public event ISwipeDetector.OnSwipeInput OnSwipe;

        public virtual void UpdateSwipeState() { }

        protected void RegisterSwipe(Vector2 swipe)
        {
            if (!IsSwipe(swipe))
                return;

            if (OnSwipe != null)
            {
                if (isVertical(swipe))
                    OnSwipe.Invoke(swipe.y > 0 ? Vector2.up : Vector2.down);
                else
                    OnSwipe.Invoke(swipe.x < 0 ? Vector2.left : Vector2.right);
            }             
        }


        protected bool isVertical(Vector2 vector) => Mathf.Abs(vector.y) > Mathf.Abs(vector.x);
        public bool IsSwipe(Vector2 swipeDelta) => swipeDelta.magnitude >= SWIPE_DEAD_ZONE;
    }

}