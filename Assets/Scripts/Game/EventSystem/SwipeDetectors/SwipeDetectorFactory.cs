﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Game.EventSystem.SwipeDetectors
{
    public class SwipeDetectorFactory
    {
        public static SwipeDetector GetSwipeDetector()
        {
            if (Application.isMobilePlatform)
            {
                return new MobileSwipeDetector();
            }
            else if (Application.isEditor)
            {
                return new PcSwipeDetector();
            }

            return null;
        }
    }
}
