using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class MainCamera : MonoBehaviour
    {
        void Awake()
        {
            GetComponent<Camera>().orthographicSize = Screen.height/2;
            transform.position = new Vector3(0, 0, transform.position.z);
        }
    }
}
