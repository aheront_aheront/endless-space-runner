using UnityEngine;
using System;

namespace Assets.Scripts.Game.Player
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class Player : MonoBehaviour
    {
        private EventSystem.EventSystem eventSystem;
        private float screenLeftSideX => Camera.main.transform.position.x - Screen.width / 2;
        private float minShipXPosition => screenLeftSideX + Screen.width / Roads.NUMBER_OF_ROADS;
        private float maxShipXPosition => screenLeftSideX + Screen.width * (1 - 1f/ Roads.NUMBER_OF_ROADS);
        private float middleRoadX => screenLeftSideX + Screen.width/2;
        private float ShipY => Camera.main.transform.position.y - Camera.main.orthographicSize + Screen.height/4;

        private void Scale(float scale) => transform.localScale = Vector3.Scale(transform.localScale, new Vector3(scale, scale, 1));
        private void Awake()
        {
            this.Scale((Roads.RoadWidth * 3 / 5) / GetComponent<BoxCollider2D>().size.x);
        }
        private void Start()
        {
            transform.position = new Vector2(middleRoadX, ShipY);
            eventSystem = EventSystem.EventSystem.instance;
            eventSystem.SwipeDetector.OnSwipe += OnSwipe;
        }

        private void OnSwipe(Vector2 direction)
        {
            if (isHorizontal(direction))
            {
                if(direction.x >= 0)
                    SlideRight();
                else
                    SlideLeft();
                
            }
        }

        private bool isHorizontal(Vector2 vector) => Math.Abs(vector.x) > Math.Abs(vector.y);
        private bool CanSlideLeft() => transform.position.x - Roads.RoadWidth > minShipXPosition;
        private void SlideLeft() 
        {
            //TODO right side shooting
            if (CanSlideLeft())
            {
            //TODO Animation
            transform.Translate(new Vector2(-Roads.RoadWidth, 0));
            }
        }
        private bool CanSlideRight() => transform.position.x + Roads.RoadWidth < maxShipXPosition;
        private void SlideRight() 
        {
            //TODO left side shooting
            if (CanSlideRight())
            {
                //TODO Animation
                transform.Translate(new Vector2(Roads.RoadWidth, 0));
            }
        }
        private void OnDisable()
        {
            eventSystem.SwipeDetector.OnSwipe -= OnSwipe;
        }

        private void OnEnable()
        {
            if(eventSystem != null)
                eventSystem.SwipeDetector.OnSwipe += OnSwipe;
        }
    }
}