﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class Roads
    {
        public const int NUMBER_OF_ROADS = 5;
        public static float RoadWidth => Screen.width / NUMBER_OF_ROADS;
    }
}
